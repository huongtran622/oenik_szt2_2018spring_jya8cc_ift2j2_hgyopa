﻿// <copyright file="RestaurantRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Implement the Data Reposity interface
    /// </summary>
    public class RestaurantRepo : IRestaurantRepo
    {
        // Declare class itself
        private static RestaurantRepo repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestaurantRepo"/> class.
        /// The constructor
        /// </summary>
        private RestaurantRepo()
        {
            this.ED = new RestaurantDbEntities();
        }

        /// <summary>
        /// Gets or sets the DB entities
        /// </summary>
        public RestaurantDbEntities ED { get; set; }

        /// <summary>
        /// Gets or sets the Data reposity
        /// </summary>
        /// <returns>get all data itself</returns>
        public static RestaurantRepo Get()
        {
            if (repo != null)
            {
                return repo;
            }
            else
            {
                repo = new RestaurantRepo();
                return repo;
            }
        }

        /// <summary>
        /// All the categories in the database
        /// </summary>
        /// <returns>IQueryable of Categories</returns>
        public IQueryable<Category> GetAllCategories()
        {
            return this.ED.Category;
        }

        /// <summary>
        /// All Meals in the database
        /// </summary>
        /// <returns>IQueryable of Meals</returns>
        public IQueryable<Meal> GetAllMeals()
        {
            return this.ED.Meal;
        }

        /// <summary>
        /// All Waiters in the database
        /// </summary>
        /// <returns>IQueryable of Waiters</returns>
        public IQueryable<Waiter> GetAllWaiters()
        {
            return this.ED.Waiter;
        }

        /// <summary>
        /// All Orders in the database
        /// </summary>
        /// <returns>IQueryable of Orders</returns>
        public IQueryable<Order> GetAllOrders()
        {
            return this.ED.Order;
        }

        /// <summary>
        /// All MealOrders in the database
        /// </summary>
        /// <returns>IQueryable of MealOrders</returns>
        public IQueryable<MealOrder> GetAllMealOrders()
        {
            return this.ED.MealOrder;
        }

        /// <summary>
        /// Add new category into the database
        /// </summary>
        /// <param name="category">object</param>
        public void AddCategory(Category category)
        {
            this.ED.Category.Add(category);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Delete the category in the database
        /// </summary>
        /// <param name="category">object</param>
        public void DeleteCategory(Category category)
        {
            this.ED.Category.Remove(category);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Add new meal into the database
        /// </summary>
        /// <param name="newMeal">object</param>
        public void AddMeal(Meal newMeal)
        {
            this.ED.Meal.Add(newMeal);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Edit the Meal in the database
        /// </summary>
        /// <param name="meal">object</param>
        public void EditMeal(Meal meal)
        {
            var item = this.ED.Meal.Single(x => x.Id == meal.Id);
            if (item != null)
            {
                item.Name = meal.Name;
                item.Price = meal.Price;
                item.Description = meal.Description;
                item.CategoryId = meal.CategoryId;
            }

            this.ED.SaveChanges();
        }

        /// <summary>
        /// Delete the meal in the database
        /// </summary>
        /// <param name="meal">object</param>
        public void DeleteMeal(Meal meal)
        {
            this.ED.Meal.Remove(meal);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Add new Order into the database
        /// </summary>
        /// <param name="order">object</param>
        public void AddOrder(Order order)
        {
            this.ED.Order.Add(order);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Edit the Order in the database
        /// </summary>
        /// <param name="order">object</param>
        public void EditOrder(Order order)
        {
            var item = this.ED.Order.Single(x => x.Id == order.Id);
            if (item != null)
            {
                item.WaiterId = order.WaiterId;
                item.TotalPrice = order.TotalPrice;
                item.Tip = order.Tip;
                item.PaymentType = order.PaymentType;
                item.PaymentStatus = order.PaymentStatus;
                item.CustomerFeedback = order.CustomerFeedback;
                item.DateTime = DateTime.Now;
            }

            this.ED.SaveChanges();
        }

        /// <summary>
        /// Add new mealOrder into the database
        /// </summary>
        /// <param name="mealorder">object</param>
        public void AddMealOrder(MealOrder mealorder)
        {
            this.ED.MealOrder.Add(mealorder);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Delete the MealOrder in the database
        /// </summary>
        /// <param name="mealorder">object</param>
        public void DeleteMealOrder(MealOrder mealorder)
        {
            this.ED.MealOrder.Remove(mealorder);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Add new waiter into the database
        /// </summary>
        /// <param name="waiter">object</param>
        public void AddWaiter(Waiter waiter)
        {
            this.ED.Waiter.Add(waiter);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Edit the Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        public void EditWaiter(Waiter waiter)
        {
            var item = this.ED.Waiter.Single(x => x.Id == waiter.Id);
            if (item != null)
            {
                item.Name = waiter.Name;
                item.Phone = waiter.Phone;
                item.Address = waiter.Address;
                item.BaseSalary = waiter.BaseSalary;
                item.TotalSalary = waiter.TotalSalary;
                item.BankAccount = waiter.BankAccount;
            }

            this.ED.SaveChanges();
        }

        /// <summary>
        /// Delete the Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        public void DeleteWaiter(Waiter waiter)
        {
            this.ED.Waiter.Remove(waiter);
            this.ED.SaveChanges();
        }

        /// <summary>
        /// Edit the MealOrder in the database
        /// </summary>
        /// <param name="order">Order which we are considering</param>
        /// <param name="mealorders">All MealOrders in the orders</param>
        public void EditMealOrders(Order order, BindingList<MealOrder> mealorders)
        {
            var item = this.ED.Order.Single(x => x.Id == order.Id);
            List<MealOrder> mealordersDB = this.GetAllMealOrders().ToList();
            foreach (MealOrder mealorder in mealordersDB)
            {
                if (mealorder.OrderId == item.Id)
                {
                    this.DeleteMealOrder(mealorder);
                }
            }

            foreach (MealOrder mealorder in mealorders)
            {
                this.AddMealOrder(mealorder);
            }

            this.ED.SaveChanges();
        }
    }
}
