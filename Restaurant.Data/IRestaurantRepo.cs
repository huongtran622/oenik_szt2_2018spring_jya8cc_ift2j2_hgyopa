﻿// <copyright file="IRestaurantRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.Data
{
    using System.ComponentModel;
    using System.Linq;

    /// <summary>
    /// Data Reposity interface
    /// </summary>
    public interface IRestaurantRepo
    {
        /// <summary>
        /// Consider all the categories in the database
        /// </summary>
        /// <returns>Iqueryable of all categories</returns>
        IQueryable<Category> GetAllCategories();

        /// <summary>
        ///  Consider all the Meals in the database
        /// </summary>
        /// <returns>Iqueryable of all meals</returns>
        IQueryable<Meal> GetAllMeals();

        /// <summary>
        ///  Consider all the Waiters in the database
        /// </summary>
        /// <returns>Iqueryable of all waiters</returns>
        IQueryable<Waiter> GetAllWaiters();

        /// <summary>
        ///  Consider all the Orders in the database
        /// </summary>
        /// <returns>Iqueryable of all orders</returns>
        IQueryable<Order> GetAllOrders();

        /// <summary>
        ///  Consider all the Meal Orders in the database
        /// </summary>
        /// <returns>Iqueryable of all MealOrder</returns>
        IQueryable<MealOrder> GetAllMealOrders();

        /// <summary>
        /// Add new Category to the database
        /// </summary>
        /// <param name="category">object</param>
        void AddCategory(Category category);

        /// <summary>
        /// Delete the Category in the database
        /// </summary>
        /// <param name="category">object</param>
        void DeleteCategory(Category category);

        /// <summary>
        /// Add new meal to the database
        /// </summary>
        /// <param name="newMeal">object</param>
        void AddMeal(Meal newMeal);

        /// <summary>
        /// Edit the Meal  in the database
        /// </summary>
        /// <param name="meal">object</param>
        void EditMeal(Meal meal);

        /// <summary>
        /// Delete the Meal in the database
        /// </summary>
        /// <param name="meal">object</param>
        void DeleteMeal(Meal meal);

        /// <summary>
        /// Add new Order to the database
        /// </summary>
        /// <param name="order">object</param>
        void AddOrder(Order order);

        /// <summary>
        /// Edit Order in the database
        /// </summary>
        /// <param name="order">object</param>
        void EditOrder(Order order);

        /// <summary>
        /// Add new MealOrder to the database
        /// </summary>
        /// <param name="mealorder">object</param>
        void AddMealOrder(MealOrder mealorder);

        /// <summary>
        /// Edit the Meal Orders in the database
        /// </summary>
        /// <param name="order">Name of the order considering</param>
        /// <param name="mealorders">list of the meal orders</param>
        void EditMealOrders(Order order, BindingList<MealOrder> mealorders);

        /// <summary>
        /// Delete the MealOrder in the database
        /// </summary>
        /// <param name="mealorder">object</param>
        void DeleteMealOrder(MealOrder mealorder);

        /// <summary>
        /// Add new Waiter to the database
        /// </summary>
        /// <param name="waiter">object</param>
        void AddWaiter(Waiter waiter);

        /// <summary>
        /// Delete Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        void EditWaiter(Waiter waiter);

        /// <summary>
        /// Delete the Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        void DeleteWaiter(Waiter waiter);
    }
}
