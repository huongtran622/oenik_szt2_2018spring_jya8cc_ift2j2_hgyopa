var searchData=
[
  ['data',['Data',['../namespace_restaurant_1_1_data.html',1,'Restaurant']]],
  ['logic',['Logic',['../namespace_restaurant_1_1_logic.html',1,'Restaurant']]],
  ['properties',['Properties',['../namespace_restaurant_1_1_properties.html',1,'Restaurant']]],
  ['restaurant',['Restaurant',['../namespace_restaurant.html',1,'']]],
  ['tests',['Tests',['../namespace_restaurant_1_1_tests.html',1,'Restaurant']]],
  ['viewmodels',['ViewModels',['../namespace_restaurant_1_1_view_models.html',1,'Restaurant']]],
  ['windowslogic',['WindowsLogic',['../namespace_restaurant_1_1_windows_logic.html',1,'Restaurant']]]
];
