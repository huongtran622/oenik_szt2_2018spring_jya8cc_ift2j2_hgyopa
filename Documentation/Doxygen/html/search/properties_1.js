var searchData=
[
  ['categories',['Categories',['../class_restaurant_1_1_view_models_1_1_menu_window_v_m.html#a7a8101efe1e9c58e50759576e07aa2e6',1,'Restaurant.ViewModels.MenuWindowVM.Categories()'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a00fcfc24700c30b3da3ed52696db2be8',1,'Restaurant.ViewModels.OrderWindowVM.Categories()']]],
  ['checkbook',['CheckBook',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a60643e7e67d2c66ab9424fd3b9e882ec',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['checkcancel',['CheckCancel',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a42ff59afef8521fdae148d01619682f0',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['checksave',['CheckSave',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a1fbc41e997c9675cec150407ca948399',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['currentorder',['CurrentOrder',['../class_restaurant_1_1_order_window.html#a8f339cd6ba0d5e3bd80d79a7350f55e6',1,'Restaurant::OrderWindow']]],
  ['customerfeedback',['CustomerFeedback',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#acff15fff7eb7c6a0e808864c1cf4c37e',1,'Restaurant::ViewModels::OrderWindowVM']]]
];
