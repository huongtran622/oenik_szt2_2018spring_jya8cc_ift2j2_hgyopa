var searchData=
[
  ['oenik_5fszt2_5f2018spring_5fjya8cc_5fift2j2_5fhgyopa',['OENIK_SZT2_2018spring_JYA8CC_IFT2J2_HGYOPA',['../md__c_1__users_bagiv_source_repos_oenik_szt2_2018spring_jya8cc_ift2j2_hgyopa__r_e_a_d_m_e.html',1,'']]],
  ['onpropertychanged',['OnPropertyChanged',['../class_restaurant_1_1_view_models_1_1_bindable.html#a0f4fd22af84dedf47957906ffd80a3ce',1,'Restaurant::ViewModels::Bindable']]],
  ['order',['Order',['../class_restaurant_1_1_data_1_1_order.html',1,'Restaurant::Data']]],
  ['orderedbackground',['OrderedBackground',['../class_restaurant_1_1_windows_logic_1_1_main_window_logic.html#a8977fea825878ca3a2a12304c6ad6b12',1,'Restaurant::WindowsLogic::MainWindowLogic']]],
  ['orderid',['OrderID',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#ab934ee6337c9e448c092ab66e4b363fe',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['orders',['Orders',['../class_restaurant_1_1_view_models_1_1_main_window_v_m.html#adae66171f9c62e48ac52117eebf8de22',1,'Restaurant::ViewModels::MainWindowVM']]],
  ['orderwindow',['OrderWindow',['../class_restaurant_1_1_order_window.html',1,'Restaurant.OrderWindow'],['../class_restaurant_1_1_order_window.html#a24a0444f69db2864031582fca585276a',1,'Restaurant.OrderWindow.OrderWindow(int tableId, int idorder)'],['../class_restaurant_1_1_order_window.html#a6800ff5dd3969c48a60984be2c5c6212',1,'Restaurant.OrderWindow.OrderWindow(Order order, BindingList&lt; MealOrder &gt; mealorder)']]],
  ['orderwindowlogic',['OrderWindowLogic',['../class_restaurant_1_1_windows_logic_1_1_order_window_logic.html',1,'Restaurant.WindowsLogic.OrderWindowLogic'],['../class_restaurant_1_1_windows_logic_1_1_order_window_logic.html#a8469da8e8b60908fe2e61c0c55ac48f9',1,'Restaurant.WindowsLogic.OrderWindowLogic.OrderWindowLogic()']]],
  ['orderwindowvm',['OrderWindowVM',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html',1,'Restaurant.ViewModels.OrderWindowVM'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a83f13f3f67838d47f3ef460e6a7aef6b',1,'Restaurant.ViewModels.OrderWindowVM.OrderWindowVM()']]]
];
