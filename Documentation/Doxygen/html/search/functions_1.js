var searchData=
[
  ['calculateincome',['CalculateIncome',['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html#aa4199fcdb27558e83c84b2bdde10f61c',1,'Restaurant::WindowsLogic::ReportWindowLogic']]],
  ['calculatetotaltip',['CalculateTotalTip',['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html#a0b8d14e1c66924bbe30547ef0ab9ca0c',1,'Restaurant::WindowsLogic::ReportWindowLogic']]],
  ['chartwindow',['ChartWindow',['../class_restaurant_1_1_chart_window.html#a6fa6e6396b8a6e343b9e09af2063fe4b',1,'Restaurant::ChartWindow']]],
  ['checkexistorder',['CheckExistOrder',['../class_restaurant_1_1_windows_logic_1_1_order_window_logic.html#af26dc7bd11b58d1169e73d50f3456fa4',1,'Restaurant::WindowsLogic::OrderWindowLogic']]],
  ['clearall',['ClearAll',['../class_restaurant_1_1_windows_logic_1_1_menu_window_logic.html#aaf8775c68ce8dbedfe2fb26437470579',1,'Restaurant::WindowsLogic::MenuWindowLogic']]],
  ['clearallwaiterstextbox',['ClearAllWaitersTextBox',['../class_restaurant_1_1_windows_logic_1_1_menu_window_logic.html#a345c0d852a232d9aa2a3c56c8181ed37',1,'Restaurant::WindowsLogic::MenuWindowLogic']]],
  ['createdelegate',['CreateDelegate',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateDelegate(System.Type delegateType, object target, string handler)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateDelegate(System.Type delegateType, object target, string handler)']]],
  ['createinstance',['CreateInstance',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateInstance(System.Type type, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateInstance(System.Type type, System.Globalization.CultureInfo culture)']]]
];
