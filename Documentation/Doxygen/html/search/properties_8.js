var searchData=
[
  ['selectedcategory',['SelectedCategory',['../class_restaurant_1_1_view_models_1_1_menu_window_v_m.html#a051834bb7ba65443ab3d3c597b0e8ed2',1,'Restaurant.ViewModels.MenuWindowVM.SelectedCategory()'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a82d848b6cf764bef5c057f874f55e94d',1,'Restaurant.ViewModels.OrderWindowVM.SelectedCategory()']]],
  ['selectedmeal',['SelectedMeal',['../class_restaurant_1_1_view_models_1_1_menu_window_v_m.html#a9beba67dbd036676c265c7039515dd60',1,'Restaurant.ViewModels.MenuWindowVM.SelectedMeal()'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#aaf6e2bf926f880d65cb4940544af8942',1,'Restaurant.ViewModels.OrderWindowVM.SelectedMeal()']]],
  ['selectedmealorder',['SelectedMealOrder',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a38fb27e072744ce1a7af34cdaff7418a',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['selectedwaiter',['SelectedWaiter',['../class_restaurant_1_1_view_models_1_1_menu_window_v_m.html#ae8fe87231f4d44113ac39999ce53151e',1,'Restaurant.ViewModels.MenuWindowVM.SelectedWaiter()'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a3049427bf0145e171139444ce2b61a90',1,'Restaurant.ViewModels.OrderWindowVM.SelectedWaiter()']]]
];
