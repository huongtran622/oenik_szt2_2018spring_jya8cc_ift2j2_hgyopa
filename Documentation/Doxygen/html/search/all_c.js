var searchData=
[
  ['data',['Data',['../namespace_restaurant_1_1_data.html',1,'Restaurant']]],
  ['logic',['Logic',['../namespace_restaurant_1_1_logic.html',1,'Restaurant']]],
  ['properties',['Properties',['../namespace_restaurant_1_1_properties.html',1,'Restaurant']]],
  ['reportwindow',['ReportWindow',['../class_restaurant_1_1_report_window.html',1,'Restaurant.ReportWindow'],['../class_restaurant_1_1_report_window.html#ab88b77ebb91f55e511cc72c00d0ca256',1,'Restaurant.ReportWindow.ReportWindow()']]],
  ['reportwindowlogic',['ReportWindowLogic',['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html',1,'Restaurant.WindowsLogic.ReportWindowLogic'],['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html#a84b2a2b0101a904038c1364b61e5f167',1,'Restaurant.WindowsLogic.ReportWindowLogic.ReportWindowLogic()']]],
  ['reportwindowvm',['ReportWindowVM',['../class_restaurant_1_1_view_models_1_1_report_window_v_m.html',1,'Restaurant.ViewModels.ReportWindowVM'],['../class_restaurant_1_1_view_models_1_1_report_window_v_m.html#a554ad1afad3d11e5e18bdac77a72f8d0',1,'Restaurant.ViewModels.ReportWindowVM.ReportWindowVM()']]],
  ['restaurant',['Restaurant',['../namespace_restaurant.html',1,'']]],
  ['restaurantdbentities',['RestaurantDbEntities',['../class_restaurant_1_1_data_1_1_restaurant_db_entities.html',1,'Restaurant::Data']]],
  ['restaurantrepo',['RestaurantRepo',['../class_restaurant_1_1_data_1_1_restaurant_repo.html',1,'Restaurant::Data']]],
  ['tests',['Tests',['../namespace_restaurant_1_1_tests.html',1,'Restaurant']]],
  ['viewmodels',['ViewModels',['../namespace_restaurant_1_1_view_models.html',1,'Restaurant']]],
  ['windowslogic',['WindowsLogic',['../namespace_restaurant_1_1_windows_logic.html',1,'Restaurant']]]
];
