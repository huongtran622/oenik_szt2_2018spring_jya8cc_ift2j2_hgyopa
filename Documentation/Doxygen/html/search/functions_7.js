var searchData=
[
  ['onpropertychanged',['OnPropertyChanged',['../class_restaurant_1_1_view_models_1_1_bindable.html#a0f4fd22af84dedf47957906ffd80a3ce',1,'Restaurant::ViewModels::Bindable']]],
  ['orderedbackground',['OrderedBackground',['../class_restaurant_1_1_windows_logic_1_1_main_window_logic.html#a8977fea825878ca3a2a12304c6ad6b12',1,'Restaurant::WindowsLogic::MainWindowLogic']]],
  ['orderwindow',['OrderWindow',['../class_restaurant_1_1_order_window.html#a24a0444f69db2864031582fca585276a',1,'Restaurant.OrderWindow.OrderWindow(int tableId, int idorder)'],['../class_restaurant_1_1_order_window.html#a6800ff5dd3969c48a60984be2c5c6212',1,'Restaurant.OrderWindow.OrderWindow(Order order, BindingList&lt; MealOrder &gt; mealorder)']]],
  ['orderwindowlogic',['OrderWindowLogic',['../class_restaurant_1_1_windows_logic_1_1_order_window_logic.html#a8469da8e8b60908fe2e61c0c55ac48f9',1,'Restaurant::WindowsLogic::OrderWindowLogic']]],
  ['orderwindowvm',['OrderWindowVM',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a83f13f3f67838d47f3ef460e6a7aef6b',1,'Restaurant::ViewModels::OrderWindowVM']]]
];
