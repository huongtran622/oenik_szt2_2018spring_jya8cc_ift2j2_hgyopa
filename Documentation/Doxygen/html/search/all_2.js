var searchData=
[
  ['calculateincome',['CalculateIncome',['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html#aa4199fcdb27558e83c84b2bdde10f61c',1,'Restaurant::WindowsLogic::ReportWindowLogic']]],
  ['calculatetotaltip',['CalculateTotalTip',['../class_restaurant_1_1_windows_logic_1_1_report_window_logic.html#a0b8d14e1c66924bbe30547ef0ab9ca0c',1,'Restaurant::WindowsLogic::ReportWindowLogic']]],
  ['categories',['Categories',['../class_restaurant_1_1_view_models_1_1_menu_window_v_m.html#a7a8101efe1e9c58e50759576e07aa2e6',1,'Restaurant.ViewModels.MenuWindowVM.Categories()'],['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a00fcfc24700c30b3da3ed52696db2be8',1,'Restaurant.ViewModels.OrderWindowVM.Categories()']]],
  ['category',['Category',['../class_restaurant_1_1_data_1_1_category.html',1,'Restaurant::Data']]],
  ['chartwindow',['ChartWindow',['../class_restaurant_1_1_chart_window.html',1,'Restaurant.ChartWindow'],['../class_restaurant_1_1_chart_window.html#a6fa6e6396b8a6e343b9e09af2063fe4b',1,'Restaurant.ChartWindow.ChartWindow()']]],
  ['checkbook',['CheckBook',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a60643e7e67d2c66ab9424fd3b9e882ec',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['checkcancel',['CheckCancel',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a42ff59afef8521fdae148d01619682f0',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['checkexistorder',['CheckExistOrder',['../class_restaurant_1_1_windows_logic_1_1_order_window_logic.html#af26dc7bd11b58d1169e73d50f3456fa4',1,'Restaurant::WindowsLogic::OrderWindowLogic']]],
  ['checksave',['CheckSave',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#a1fbc41e997c9675cec150407ca948399',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['clearall',['ClearAll',['../class_restaurant_1_1_windows_logic_1_1_menu_window_logic.html#aaf8775c68ce8dbedfe2fb26437470579',1,'Restaurant::WindowsLogic::MenuWindowLogic']]],
  ['clearallwaiterstextbox',['ClearAllWaitersTextBox',['../class_restaurant_1_1_windows_logic_1_1_menu_window_logic.html#a345c0d852a232d9aa2a3c56c8181ed37',1,'Restaurant::WindowsLogic::MenuWindowLogic']]],
  ['createdelegate',['CreateDelegate',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateDelegate(System.Type delegateType, object target, string handler)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#a8ec4c37e82d9f4e867e9655f4eac3a78',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateDelegate(System.Type delegateType, object target, string handler)']]],
  ['createinstance',['CreateInstance',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateInstance(System.Type type, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#aefb7a98fceb9c287cef4756942f441d1',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.CreateInstance(System.Type type, System.Globalization.CultureInfo culture)']]],
  ['currentorder',['CurrentOrder',['../class_restaurant_1_1_order_window.html#a8f339cd6ba0d5e3bd80d79a7350f55e6',1,'Restaurant::OrderWindow']]],
  ['customerfeedback',['CustomerFeedback',['../class_restaurant_1_1_view_models_1_1_order_window_v_m.html#acff15fff7eb7c6a0e808864c1cf4c37e',1,'Restaurant::ViewModels::OrderWindowVM']]],
  ['castle_20core_20changelog',['Castle Core Changelog',['../md__c_1__users_bagiv_source_repos_oenik_szt2_2018spring_jya8cc_ift2j2_hgyopa_packages__castle_8_13df2c03dcbde1d7784bb3fff5a389b5.html',1,'']]]
];
