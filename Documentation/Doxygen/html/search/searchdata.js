var indexSectionsWithContent =
{
  0: "abcdefgimnoprstuvwx",
  1: "abcdfgimorvw",
  2: "rx",
  3: "acdegimoprstu",
  4: "acdeimopstw",
  5: "p",
  6: "cno"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "events",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Events",
  6: "Pages"
};

