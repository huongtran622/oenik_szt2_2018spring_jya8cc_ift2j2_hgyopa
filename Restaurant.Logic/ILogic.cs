﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.Logic
{
    using System.Collections.Generic;
    using Data;

    /// <summary>
    /// The logic interface
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Take from database
        /// </summary>
        /// <returns>List of all categories</returns>
        List<Category> GetAllCategories();

        /// <summary>
        /// Take from database
        /// </summary>
        /// <returns>List of all meals</returns>
        List<Meal> GetAllMeals();

        /// <summary>
        /// Take from database
        /// </summary>
        /// <returns>List of all waiter</returns>
        List<Waiter> GetAllWaiters();

        /// <summary>
        /// Take from database
        /// </summary>
        /// <returns>List of all orders</returns>
        List<Order> GetAllOrders();

        /// <summary>
        /// Take from database
        /// </summary>
        /// <returns>List of all meal orders</returns>
        List<MealOrder> GetAllMealOrders();

        /// <summary>
        /// Add meal to the database
        /// </summary>
        /// <param name="newMeal">the Meal we want to add</param>
        void AddMeal(Meal newMeal);

        /// <summary>
        /// Edit meal in the database
        /// </summary>
        /// <param name="meal">the Meal we want to edit</param>
        void EditMeal(Meal meal);

        /// <summary>
        /// Delete meal in the database
        /// </summary>
        /// <param name="meal">the Meal we want to delete</param>
        void DeleteMeal(Meal meal);

        /// <summary>
        /// Add waiter to the database
        /// </summary>
        /// <param name="newWaiter">the waiter we want to add</param>
        void AddWaiter(Waiter newWaiter);

        /// <summary>
        /// Edit waiter in the database
        /// </summary>
        /// <param name="waiter">the waiter we want to edit</param>
        void EditWaiter(Waiter waiter);

        /// <summary>
        /// Delete waiter in the database
        /// </summary>
        /// <param name="waiter">the waiter we want to delete</param>
        void DeleteWaiter(Waiter waiter);

        /// <summary>
        /// Add the Category to the database
        /// </summary>
        /// <param name="cate">The category we want to add</param>
        void AddCategory(Category cate);

        /// <summary>
        /// Delete the Category in the database
        /// </summary>
        /// <param name="cate">The category we want to delete</param>
        void DeleteCategory(Category cate);

        /// <summary>
        /// Add the Order to the database
        /// </summary>
        /// <param name="order">the order we want to add</param>
        void AddOrder(Order order);

        /// <summary>
        /// Add the Mealorder to the database
        /// </summary>
        /// <param name="mo">The mealorder we want to add</param>
        void AddMealOrder(MealOrder mo);

        /// <summary>
        /// Delete the Mealorder in the database
        /// </summary>
        /// <param name="mo">The mealorder we want to delete</param>
        void DeleteMealOrder(MealOrder mo);

        /// <summary>
        /// Check all total price of orders in the database
        /// </summary>
        /// <returns>Total income</returns>
        double GetTotalIncome();

        /// <summary>
        /// Check all tip of orders in the database
        /// </summary>
        /// <returns>Return the total tip</returns>
        double GetTotalTip();
    }
}
