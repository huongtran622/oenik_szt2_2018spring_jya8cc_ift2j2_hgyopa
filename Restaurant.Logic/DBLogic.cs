﻿// <copyright file="DBLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.Logic
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using Data;

    /// <summary>
    /// Implement the Logic interface
    /// </summary>
    public class DBLogic : ILogic
    {
        /// <summary>
        /// Call the Data
        /// </summary>
        private RestaurantRepo dataRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DBLogic"/> class.
        /// Constructor of the DBLogic class
        /// </summary>
        public DBLogic()
        {
            this.dataRepository = RestaurantRepo.Get();
        }

        /// <summary>
        /// Find all the categories in the database
        /// </summary>
        /// <returns>List of all categories</returns>
        public List<Category> GetAllCategories()
        {
            return this.dataRepository.GetAllCategories().ToList();
        }

        /// <summary>
        /// Find all meals in the database
        /// </summary>
        /// <returns>List of meals</returns>
        public List<Meal> GetAllMeals()
        {
            return this.dataRepository.GetAllMeals().ToList();
        }

        /// <summary>
        /// Find all Waiter in the database
        /// </summary>
        /// <returns>List of waiters</returns>
        public List<Waiter> GetAllWaiters()
        {
            return this.dataRepository.GetAllWaiters().ToList();
        }

        /// <summary>
        /// Find all Orders in the database
        /// </summary>
        /// <returns>List of Orders</returns>
        public List<Order> GetAllOrders()
        {
            return this.dataRepository.GetAllOrders().ToList();
        }

        /// <summary>
        /// Find all meal orders in the database
        /// </summary>
        /// <returns>List of MealOrders</returns>
        public List<MealOrder> GetAllMealOrders()
        {
            return this.dataRepository.GetAllMealOrders().ToList();
        }

        /// <summary>
        /// Add meal to the database
        /// </summary>
        /// <param name="newMeal">Meal we want to add</param>
        public void AddMeal(Meal newMeal)
        {
            if (newMeal.Name == null || newMeal.Price == 0 || newMeal.CategoryId == 0)
            {
                throw new ApplicationException("Please specify the name, price and category");
            }

            this.dataRepository.AddMeal(newMeal);
        }

        /// <summary>
        /// Edit meal in the database
        /// </summary>
        /// <param name="meal">Meal we want to edit</param>
        public void EditMeal(Meal meal)
        {
            this.dataRepository.EditMeal(meal);
        }

        /// <summary>
        /// Delete the meal in the database
        /// </summary>
        /// <param name="meal">Meal we are considered</param>
        public void DeleteMeal(Meal meal)
        {
            this.dataRepository.DeleteMeal(meal);
        }

        /// <summary>
        /// Add new waiter into the database
        /// </summary>
        /// <param name="newWaiter">object</param>
        public void AddWaiter(Waiter newWaiter)
        {
            if (newWaiter.Name == null || newWaiter.BaseSalary == 0)
            {
                throw new ApplicationException("Cannot insert a waiter without name or base salary");
            }

            this.dataRepository.AddWaiter(newWaiter);
        }

        /// <summary>
        /// Edit Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        public void EditWaiter(Waiter waiter)
        {
            if (waiter == null)
            {
                throw new ArgumentNullException(nameof(waiter));
            }

            this.dataRepository.EditWaiter(waiter);
        }

        /// <summary>
        /// Delete Waiter in the database
        /// </summary>
        /// <param name="waiter">object</param>
        public void DeleteWaiter(Waiter waiter)
        {
            if (waiter == null)
            {
                throw new ArgumentNullException(nameof(waiter));
            }

            if (!this.GetAllWaiters().Contains(waiter))
            {
                throw new ApplicationException("Cannot delete not existing waiter");
            }

            this.dataRepository.DeleteWaiter(waiter);
        }

        /// <summary>
        /// Add new category into the database
        /// </summary>
        /// <param name="cate">object</param>
        public void AddCategory(Category cate)
        {
            if (cate == null)
            {
                throw new ArgumentNullException(nameof(cate));
            }

            if (cate.Name == null)
            {
                throw new ApplicationException("Cannot insert a category without name");
            }

            if (this.GetAllCategories().Find(x => x.Name == cate.Name) != null)
            {
                throw new ApplicationException("Cannot duplicate category");
            }

            this.dataRepository.AddCategory(cate);
        }

        /// <summary>
        /// Delete the category in the database
        /// </summary>
        /// <param name="cate">object</param>
        public void DeleteCategory(Category cate)
        {
            this.dataRepository.DeleteCategory(cate);
        }

        /// <summary>
        /// add new order into the database
        /// </summary>
        /// <param name="order">object</param>
        public void AddOrder(Order order)
        {
            if (order.TotalPrice == 0 || order.WaiterId == 0 || order.TableId == 0)
            {
                throw new ApplicationException("Please specify the name, status, total price, waiter and the table");
            }

            this.dataRepository.AddOrder(order);
        }

        /// <summary>
        /// Edit the order in the database
        /// </summary>
        /// <param name="order">object</param>
        public void EditOrder(Order order)
        {
            this.dataRepository.EditOrder(order);
        }

        /// <summary>
        /// Add the new mealorder into the database
        /// </summary>
        /// <param name="mo">object</param>
        public void AddMealOrder(MealOrder mo)
        {
            if (mo.MealId == 0 || mo.Quantity == 0 || mo.OrderId == 0 || mo.Total_Price == 0)
            {
                throw new ApplicationException("Cannot insert a MealOrder without a mealId, quantity, orderId and total price");
            }

            this.dataRepository.AddMealOrder(mo);
        }

        /// <summary>
        /// Delete the Mealorder in the database
        /// </summary>
        /// <param name="mo">object</param>
        public void DeleteMealOrder(MealOrder mo)
        {
            this.dataRepository.DeleteMealOrder(mo);
        }

        /// <summary>
        /// Update the Mealorders in the database
        /// </summary>
        /// <param name="currentOrder">The order we are considering</param>
        /// <param name="mealorders">the meal order objects</param>
        public void UpdateMealOrders(Order currentOrder, BindingList<MealOrder> mealorders)
        {
            this.dataRepository.EditMealOrders(currentOrder, mealorders);
        }

        /// <summary>
        /// Consider all the Total Price of the all orders in the database
        /// </summary>
        /// <returns>Total income</returns>
        public double GetTotalIncome()
        {
            double income = 0;
            foreach (Order order in this.GetAllOrders())
            {
                income += order.TotalPrice;
            }

            return income;
        }

        /// <summary>
        /// Consider all the tip  of the all orders in the database
        /// </summary>
        /// <returns>Total tip</returns>
        public double GetTotalTip()
        {
            double tip = 0;
            foreach (Order order in this.GetAllOrders())
            {
                tip += (double)order.Tip;
            }

            return tip;
        }
    }
}