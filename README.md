# OENIK_SZT2_2018spring_JYA8CC_IFT2J2_HGYOPA
Software Engineering Home Project 
GROUP B
	SOFTWARE FOR RESTAURANTS
- Software Engineering II - 2018 Spring-

Description: 
We will create a software for the restaurants to have all the processes
The software contains these features below 

1. Tables's place. 
Each table has its own number to distinguish.
When the table is clicked, it will change its background to identify that the table is no longer available. 
When Invoice is clicked, the table symbol changes back to the initial color, that means the table is back to available again. 

2. Food Ordering 
We have a menu that contains all the food we have in the restaurant. 
Click to the table choosed, then we will pick up the food which is in the menu. Choose the food that the customers ordered, fill up all other information in Order page!
Click Book. If changing anything, click Save. If customers paid, click Invoice and Print!

3. Invoice
Generate automatically from Order selected. When Print is clicked, the table is free and back to first screen with initial state, the payment status of the order changed to yes!

4. Report
- Show all the orders and tip each waiters. 
- Total income
- Total tip

5. Menu and Waiter Editor
To Add/Edit/Delete the Foods in the Menu and the Waiters.

6. Other functions:  
- A chart to see how the revenue develop each day
- A chart to show the tip of each waiter


