﻿// <copyright file="InvoiceWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System.Windows;
    using ViewModels;

    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        // Call the viewmodel of Order Window
        private OrderWindowVM viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceWindow"/> class.
        /// initializes with viewmodel
        /// </summary>
        /// <param name="vm">OrderWindowVM</param>
        public InvoiceWindow(OrderWindowVM vm)
        {
            this.InitializeComponent();
            this.viewModel = vm;
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// If the Print button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// If the Back button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
