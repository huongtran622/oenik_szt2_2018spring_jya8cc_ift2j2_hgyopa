﻿// <copyright file="ReportWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System.Windows;
    using Logic;
    using ViewModels;
    using WindowsLogic;

    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        // Call the viewmodel
        private ReportWindowVM viewModel;

        // Call the logic in logic layer
        private DBLogic logic;

        // Call the logic of the ReportWindow
        private ReportWindowLogic windowLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindow"/> class.
        /// Initializes the window
        /// </summary>
        public ReportWindow()
        {
            this.InitializeComponent();
            this.logic = new DBLogic();
            this.viewModel = new ReportWindowVM();
            this.windowLogic = new ReportWindowLogic(this.viewModel);
            this.viewModel.AllOrders = this.logic.GetAllOrders();
            this.viewModel.Income = this.windowLogic.CalculateIncome();
            this.viewModel.TotalTip = this.windowLogic.CalculateTotalTip();
            this.windowLogic.DisplayWaiter();
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// If the Exit button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
