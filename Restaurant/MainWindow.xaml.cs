﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Logic;
    using ViewModels;
    using WindowsLogic;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // the idorder
        private int idorder;

        // Declare the logic in logic layer
        private DBLogic logic;

        // call the viewmodel
        private MainWindowVM viewModel;

        // call the logic of the MainWindow
        private MainWindowLogic mainLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// Constructor for MainWindow
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.idorder = 0;
            this.mainLogic = new MainWindowLogic(this.viewModel);
        }

        /// <summary>
        /// When the window is loaded
        /// </summary>
        /// <param name="sender">window</param>
        /// <param name="e">event handler</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.viewModel = new MainWindowVM();
            this.logic = new DBLogic();
        }

        /// <summary>
        /// If the table button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void TableButton_Click(object sender, RoutedEventArgs e)
        {
            Button tableButton = sender as Button;

            Brush brush = this.Resources["ButtonBackground"] as Brush;
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Resources/t.png", UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            int tableId = int.Parse(tableButton.Content.ToString());

            if (tableButton.Background == brush)
            {
                OrderWindow orderWindow = new OrderWindow(tableId, this.idorder);
                orderWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Hide();
                if (orderWindow.ShowDialog() == true)
                {
                    tableButton.Background = ib;

                    // can not use logic in mainLogic because of this order should be increased
                    this.idorder++;
                    orderWindow.Close();
                    this.Show();
                }
                else
                {
                    orderWindow.Close();
                    this.Show();
                }
            }

            // if the table have background different like initial backgound then when it is click, it should show the order
            // which is already there (EDIT)
            else
            {
                this.mainLogic.OrderedBackground(tableButton, brush, tableId);
            }
        }

        /// <summary>
        /// When click to the Menu button
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void MenuEditor_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow menuWindow = new MenuWindow();
            menuWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Hide();
            if (menuWindow.ShowDialog() == false)
            {
                menuWindow.Close();
                this.Show();
            }
        }

        /// <summary>
        /// When click to the Exit button
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// When click to the Report button
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Report_Click(object sender, RoutedEventArgs e)
        {
            if (this.logic.GetAllOrders().Count > 0)
            {
                ReportWindow win = new ReportWindow();
                win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Hide();
                if (win.ShowDialog() == false)
                {
                    win.Close();
                    this.Show();
                }
            }
            else
            {
                MessageWindow ms = new MessageWindow("      There is no orders!");
                ms.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                ms.Show();
            }
        }

        /// <summary>
        /// When click to the Chart button
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void ChartButton_Click(object sender, RoutedEventArgs e)
        {
            ChartWindow w = new ChartWindow();
            this.Hide();
            if (w.ShowDialog() == false)
            {
                w.Close();
                this.Show();
            }
        }
    }
}
