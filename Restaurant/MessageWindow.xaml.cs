﻿// <copyright file="MessageWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System.Windows;
    using ViewModels;

    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window
    {
        // Call the viewmodel
        private MessageWindowVM viewmodel;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindow"/> class.
        /// Start with the initial window
        /// </summary>
        /// <param name="message">the text wanted to show on the window</param>
        public MessageWindow(string message)
        {
            this.InitializeComponent();
            this.viewmodel = new MessageWindowVM();
            this.viewmodel.Message = message;
            this.DataContext = this.viewmodel;
        }

        /// <summary>
        /// If the ok button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
