﻿// <copyright file="ReportWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    using System.Collections.Generic;
    using Data;

    /// <summary>
    /// Report Winow VM
    /// </summary>
    public class ReportWindowVM : Bindable
    {
        // Declare the total price we got from all orders in the database
        private double income;

        // Declare the total tip we got from orders in database
        private double totalTip;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindowVM"/> class.
        /// Constructor of the ReportWindowVM
        /// </summary>
        public ReportWindowVM()
        {
            this.AllOrders = new List<Order>();
            this.AllWaiters = new List<Waiter>();
        }

        /// <summary>
        /// Gets or sets the list of orders in the database
        /// </summary>
        public List<Order> AllOrders { get; set; }

        /// <summary>
        /// Gets or sets the list of waiters in the database
        /// </summary>
        public List<Waiter> AllWaiters { get; set; }

        /// <summary>
        /// Gets or sets the income
        /// </summary>
        public double Income
        {
            get
            {
                return this.income;
            }

            set
            {
                this.SetProperty(ref this.income, value);
            }
        }

        /// <summary>
        /// Gets or sets the total tip
        /// </summary>
        public double TotalTip
        {
            get
            {
                return this.totalTip;
            }

            set
            {
                this.SetProperty(ref this.totalTip, value);
            }
        }
    }
}
