﻿// <copyright file="MessageWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    /// <summary>
    /// Viewmodel for Message Window
    /// </summary>
    public class MessageWindowVM : Bindable
    {
        // Message to show on the window
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindowVM"/> class.
        /// Constructor takes no parameters
        /// </summary>
        public MessageWindowVM()
        {
        }

        /// <summary>
        /// Gets or sets Message on the window
        /// </summary>
        public string Message
        {
            get
            {
                return this.message;
            }

            set
            {
                this.SetProperty(ref this.message, value);
            }
        }
    }
}
