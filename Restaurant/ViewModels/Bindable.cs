﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Use for binding
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Event handler for property changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// On the property changed, it invoked the even
        /// </summary>
        /// <param name="name">name of property</param>
        protected void OnPropertyChanged(string name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Set the value after changed
        /// </summary>
        /// <typeparam name="T">reference</typeparam>
        /// <param name="field">property which you want to set</param>
        /// <param name="value">its value</param>
        /// <param name="name">name</param>
        protected void SetProperty<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            field = value;
            this.OnPropertyChanged(name);
        }
    }
}
