﻿// <copyright file="OrderWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using Data;
    using Logic;

    /// <summary>
    /// Order Window ViewModel
    /// </summary>
    public class OrderWindowVM : Bindable
    {
        // Call the Logic Layer
        private DBLogic dbLogic;

        // To check if the order is able to save
        private bool checkSave;

        // check if the order is cancel
        private bool checkCancel;

        // check if the order is booked or not
        private bool checkBook;

        // The selected Category property
        private Category selectedCategory;

        // Declare the selected meal
        private Meal selectedMeal;

        // Declare the Selected meal order
        private MealOrder selectedMealOrder;

        // Declare the selected Waiter
        private Waiter selectedWaiter;

        // Declare the total Price of Order
        private double totalPrice;

        // The table id of this order
        private int tableID;

        // the order id of the order
        private int orderID;

        // the payment status of the order
        private bool paymentStatus;

        // the tip of the order
        private double tipOrder;

        // the payment type of the order
        private string paymentType;

        // the customer feedback of the order
        private string customerFeedback;

        // the datetime of the order
        private string datetime;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderWindowVM"/> class.
        /// The constructor of the OrderWindowVM
        /// </summary>
        public OrderWindowVM()
        {
            this.dbLogic = new DBLogic();
            this.Categories = this.dbLogic.GetAllCategories();
            this.Meals = this.dbLogic.GetAllMeals();
            this.Waiters = this.dbLogic.GetAllWaiters();
            this.MealOrders = new BindingList<MealOrder>();
            this.CheckSave = false;
            this.checkCancel = true;
            this.CheckBook = true;
        }

        /// <summary>
        /// Gets or sets List of Categories
        /// </summary>
        public List<Category> Categories { get; set; }

        /// <summary>
        /// Gets or sets list of Meals
        /// </summary>
        public List<Meal> Meals { get; set; }

        /// <summary>
        /// Gets or sets list of waiters
        /// </summary>
        public List<Waiter> Waiters { get; set; }

        /// <summary>
        /// Gets or sets list of Meal Orders
        /// </summary>
        public BindingList<MealOrder> MealOrders { get; set; }

        /// <summary>
        /// Gets or sets the Selected Category
        /// </summary>
        public Category SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets the Selected Meal
        /// </summary>
        public Meal SelectedMeal
        {
            get
            {
                return this.selectedMeal;
            }

            set
            {
                this.SetProperty(ref this.selectedMeal, value);
            }
        }

        /// <summary>
        /// Gets or sets the Total Price
        /// </summary>
        public double TotalPrice
        {
            get
            {
                return this.totalPrice;
            }

            set
            {
                this.SetProperty(ref this.totalPrice, value);
            }
        }

        /// <summary>
        /// Gets or sets the table ID
        /// </summary>
        public int TableID
        {
            get
            {
                return this.tableID;
            }

            set
            {
                this.SetProperty(ref this.tableID, value);
            }
        }

        /// <summary>
        /// Gets or sets the Order ID
        /// </summary>
        public int OrderID
        {
            get
            {
                return this.orderID;
            }

            set
            {
                this.SetProperty(ref this.orderID, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order is payed
        /// </summary>
        public bool PaymentStatus
        {
            get
            {
                return this.paymentStatus;
            }

            set
            {
                this.SetProperty(ref this.paymentStatus, value);
            }
        }

        /// <summary>
        /// Gets or sets the Tip
        /// </summary>
        public double TipOrder
        {
            get
            {
                return this.tipOrder;
            }

            set
            {
                this.SetProperty(ref this.tipOrder, value);
            }
        }

        /// <summary>
        /// Gets or sets the Payment Type
        /// </summary>
        public string PaymentType
        {
            get
            {
                return this.paymentType;
            }

            set
            {
                this.SetProperty(ref this.paymentType, value);
            }
        }

        /// <summary>
        /// Gets or sets the Customer Feedback
        /// </summary>
        public string CustomerFeedback
        {
            get
            {
                return this.customerFeedback;
            }

            set
            {
                this.SetProperty(ref this.customerFeedback, value);
            }
        }

        /// <summary>
        /// Gets or sets the Datetime
        /// </summary>
        public string Datetime
        {
            get
            {
                return this.datetime;
            }

            set
            {
                this.SetProperty(ref this.datetime, value);
            }
        }

        /// <summary>
        /// Gets or sets the Selected meal order
        /// </summary>
        public MealOrder SelectedMealOrder
        {
            get
            {
                return this.selectedMealOrder;
            }

            set
            {
                this.SetProperty(ref this.selectedMealOrder, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected waiter
        /// </summary>
        public Waiter SelectedWaiter
        {
            get
            {
                return this.selectedWaiter;
            }

            set
            {
                this.SetProperty(ref this.selectedWaiter, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order is clicked SAVE or not
        /// </summary>
        public bool CheckSave
        {
            get
            {
                return this.checkSave;
            }

            set
            {
                this.SetProperty(ref this.checkSave, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order is clicked Cancel or not
        /// </summary>
        public bool CheckCancel
        {
            get
            {
                return this.checkCancel;
            }

            set
            {
                this.SetProperty(ref this.checkCancel, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order is clicked BOOK or not
        /// </summary>
        public bool CheckBook
        {
            get
            {
                return this.checkBook;
            }

            set
            {
                this.checkBook = value;
            }
        }
    }
}