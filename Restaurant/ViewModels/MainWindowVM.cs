﻿// <copyright file="MainWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    using System.Collections.ObjectModel;
    using Data;

    /// <summary>
    /// Viewmodel for Main Window
    /// </summary>
    public class MainWindowVM : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowVM"/> class.
        /// Constructor of Main Window VM
        /// </summary>
        public MainWindowVM()
        {
            this.Orders = new ObservableCollection<Order>();
        }

        /// <summary>
        /// Gets or sets the List of All Orders that MainWindow Stores
        /// </summary>
        public ObservableCollection<Order> Orders { get; set; }
    }
}
