﻿// <copyright file="MenuWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.ViewModels
{
    using System.ComponentModel;
    using Data;

    /// <summary>
    /// Menu Window Viewmodel
    /// </summary>
    public class MenuWindowVM : Bindable
    {
        // Meal selected
        private Meal selectedMeal;

        // Seleted Category
        private Category selectedCategory;

        // Selected Waiter
        private Waiter selectedWaiter;

        // The id of the meal
        private int idMeal;

        // The name of the meal
        private string mealName;

        // The description of the meal
        private string desc;

        // The id category of the meal
        private int idCate;

        // The price of the meal
        private double priceMeal;

        // The Id of the waiter
        private int wID;

        // The name of the waiter
        private string wName;

        // The phone of the waiter
        private string wPhone;

        // the Address of the waiter
        private string wAddress;

        // The Bank account of the waiter
        private string wBankAcc;

        // The based salary per hour of the waiter
        private double wBasedSalary;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindowVM"/> class.
        /// Constructor of Menu Window VM
        /// </summary>
        public MenuWindowVM()
        {
            this.Categories = new BindingList<Category>();
            this.Meals = new BindingList<Meal>();
            this.AllWaiters = new BindingList<Waiter>();
        }

        /// <summary>
        /// Gets or sets list of categories
        /// </summary>
        public BindingList<Category> Categories { get; set; }

        /// <summary>
        /// Gets or sets list of meals
        /// </summary>
        public BindingList<Meal> Meals { get; set; }

        /// <summary>
        /// Gets or sets list of waiters
        /// </summary>
        public BindingList<Waiter> AllWaiters { get; set; }

        /// <summary>
        /// Gets or sets the Meal selected
        /// </summary>
        public Meal SelectedMeal
        {
            get
            {
                return this.selectedMeal;
            }

            set
            {
                this.SetProperty(ref this.selectedMeal, value);
            }
        }

        /// <summary>
        /// Gets or sets the Category selected
        /// </summary>
        public Category SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }

            set
            {
                this.SetProperty(ref this.selectedCategory, value);
            }
        }

        /// <summary>
        /// Gets or sets the id of meal
        /// </summary>
        public int IdMeal
        {
            get
            {
                return this.idMeal;
            }

            set
            {
                this.SetProperty(ref this.idMeal, value);
            }
        }

        /// <summary>
        /// Gets or sets the meal name
        /// </summary>
        public string MealName
        {
            get
            {
                return this.mealName;
            }

            set
            {
                this.SetProperty(ref this.mealName, value);
            }
        }

        /// <summary>
        /// Gets or sets the Description of meal
        /// </summary>
        public string Desc
        {
            get
            {
                return this.desc;
            }

            set
            {
                this.SetProperty(ref this.desc, value);
            }
        }

        /// <summary>
        /// Gets or sets the id category of meal
        /// </summary>
        public int IdCate
        {
            get
            {
                return this.idCate;
            }

            set
            {
                this.SetProperty(ref this.idCate, value);
            }
        }

        /// <summary>
        /// Gets or sets the price of meal
        /// </summary>
        public double PriceMeal
        {
            get
            {
                return this.priceMeal;
            }

            set
            {
                this.SetProperty(ref this.priceMeal, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected waiter
        /// </summary>
        public Waiter SelectedWaiter
        {
            get
            {
                return this.selectedWaiter;
            }

            set
            {
                this.SetProperty(ref this.selectedWaiter, value);
            }
        }

        /// <summary>
        /// Gets or sets the ID of the waiter
        /// </summary>
        public int WID
        {
            get
            {
                return this.wID;
            }

            set
            {
                this.SetProperty(ref this.wID, value);
            }
        }

        /// <summary>
        /// Gets or sets the Waiter Name
        /// </summary>
        public string WName
        {
            get
            {
                return this.wName;
            }

            set
            {
                this.SetProperty(ref this.wName, value);
            }
        }

        /// <summary>
        /// Gets or sets the Waiter phone
        /// </summary>
        public string WPhone
        {
            get
            {
                return this.wPhone;
            }

            set
            {
                this.SetProperty(ref this.wPhone, value);
            }
        }

        /// <summary>
        /// Gets or sets the Waiter's Address
        /// </summary>
        public string WAddress
        {
            get
            {
                return this.wAddress;
            }

            set
            {
                this.SetProperty(ref this.wAddress, value);
            }
        }

        /// <summary>
        /// Gets or sets the Waiter's Bank Account
        /// </summary>
        public string WBankAcc
        {
            get
            {
                return this.wBankAcc;
            }

            set
            {
                this.SetProperty(ref this.wBankAcc, value);
            }
        }

        /// <summary>
        /// Gets or sets the waiter's Based salary
        /// </summary>
        public double WBasedSalary
        {
            get
            {
                return this.wBasedSalary;
            }

            set
            {
                this.SetProperty(ref this.wBasedSalary, value);
            }
        }
    }
}
