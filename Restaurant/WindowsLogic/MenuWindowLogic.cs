﻿// <copyright file="MenuWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.WindowsLogic
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using Data;
    using Logic;
    using ViewModels;

    /// <summary>
    /// Logic for Menu Window
    /// </summary>
    public class MenuWindowLogic
    {
        // Call the viewmodel of Menu Window
        private MenuWindowVM viewModel;

        // Call the logic in logic layer
        private DBLogic dbLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindowLogic"/> class.
        /// Constructor takes MenuWindowVM vm as a parameter
        /// </summary>
        /// <param name="vm">the MenuWindowVM</param>
        public MenuWindowLogic(MenuWindowVM vm)
        {
            this.viewModel = vm;
            this.dbLogic = new DBLogic();
        }

        /// <summary>
        /// Display the selected meal
        /// </summary>
        public void DisplaySelectedMeal()
        {
            if (this.viewModel.SelectedMeal == null)
            {
                return;
            }

            this.viewModel.IdMeal = this.viewModel.SelectedMeal.Id;
            this.viewModel.MealName = this.viewModel.SelectedMeal.Name;
            this.viewModel.Desc = this.viewModel.SelectedMeal.Description;
            this.viewModel.IdCate = this.viewModel.SelectedMeal.CategoryId;
            this.viewModel.PriceMeal = this.viewModel.SelectedMeal.Price;
        }

        /// <summary>
        /// Clear all the textbox in the window
        /// </summary>
        /// <param name="id">id textbox</param>
        /// <param name="name">name textbox</param>
        /// <param name="desc">description textbox</param>
        /// <param name="idcate">id category textbox</param>
        /// <param name="price">price textbox</param>
        public void ClearAll(TextBox id, TextBox name, TextBox desc, TextBox idcate, TextBox price)
        {
            id.Clear();
            name.Clear();
            desc.Clear();
            idcate.Clear();
            price.Clear();
        }

        /// <summary>
        /// Get All the Meals ID in the List of Meals in viewmodel
        /// </summary>
        /// <returns>List of integer numbers</returns>
        public List<int> GetAllMealsID()
        {
            List<int> ids = new List<int>();
            foreach (Meal m in this.viewModel.Meals)
            {
                ids.Add(m.Id);
            }

            return ids;
        }

        /// <summary>
        /// Add considering meal to the database and viewmodel Meals list
        /// </summary>
        public void AddMeal()
        {
            if (this.viewModel.MealName == null || this.viewModel.IdCate == 0 || this.viewModel.PriceMeal == 0)
            {
                MessageWindow ms = new MessageWindow("     Please fill it all out!");
                ms.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                ms.Show();
            }
            else
            {
                bool check = true;
                foreach (int id in this.GetAllMealsID())
                {
                    if (this.viewModel.IdMeal == id)
                    {
                        check = false;
                    }
                }

                if (check == false)
                {
                    MessageWindow ms2 = new MessageWindow("Id is not available! Try another");
                    ms2.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    ms2.Show();
                }
                else
                {
                    Meal m = new Meal();
                    m.Name = this.viewModel.MealName;
                    m.Description = this.viewModel.Desc;
                    m.CategoryId = this.viewModel.IdCate;
                    m.Price = this.viewModel.PriceMeal;
                    this.viewModel.Meals.Add(m);
                    this.dbLogic.AddMeal(m);
                }
            }
        }

        /// <summary>
        /// Edit the selected Meal
        /// </summary>
        /// <param name="listbox">Name of the Meals listbox</param>
        public void EditMeal(ListBox listbox)
        {
            if (this.viewModel.SelectedMeal == null)
            {
                return;
            }
            else
            {
                Meal m = new Meal();
                m.Id = this.viewModel.IdMeal;
                m.Name = this.viewModel.MealName;
                m.Description = this.viewModel.Desc;
                m.CategoryId = this.viewModel.IdCate;
                m.Price = this.viewModel.PriceMeal;
                int index = listbox.SelectedIndex;
                this.viewModel.Meals.RemoveAt(index);
                this.viewModel.Meals.Insert(index, m);
                this.dbLogic.EditMeal(m);
            }
        }

        /// <summary>
        /// Delete meal in the viewmodel and in the database as well
        /// </summary>
        public void DeleteMeal()
        {
            if (this.viewModel.SelectedMeal == null)
            {
                return;
            }

            Meal m = this.viewModel.SelectedMeal;
            this.viewModel.Meals.Remove(this.viewModel.SelectedMeal);
            this.dbLogic.DeleteMeal(m);
        }

        /*
         * This will be the logic for Waiter editor
         */

        /// <summary>
        /// Clear all the textbox in the waiter tab control
        /// </summary>
        /// <param name="id">id of the waiter</param>
        /// <param name="name">name of the waiter</param>
        /// <param name="phone">phone of the waiter</param>
        /// <param name="address">address of the waiter</param>
        /// <param name="bankAcc">bank account of the waiter</param>
        /// <param name="basedsalary">based salary of the waiter</param>
        public void ClearAllWaitersTextBox(TextBox id, TextBox name, TextBox phone, TextBox address, TextBox bankAcc, TextBox basedsalary)
        {
            id.Clear();
            name.Clear();
            phone.Clear();
            address.Clear();
            bankAcc.Clear();
            basedsalary.Clear();
        }

        /// <summary>
        /// To Display the selected waiter
        /// </summary>
        public void DisplaySelectedWaiter()
        {
            if (this.viewModel.SelectedWaiter == null)
            {
                return;
            }

            this.viewModel.WID = this.viewModel.SelectedWaiter.Id;
            this.viewModel.WName = this.viewModel.SelectedWaiter.Name;
            this.viewModel.WPhone = this.viewModel.SelectedWaiter.Phone;
            this.viewModel.WAddress = this.viewModel.SelectedWaiter.Address;
            this.viewModel.WBankAcc = this.viewModel.SelectedWaiter.BankAccount;
            this.viewModel.WBasedSalary = this.viewModel.SelectedWaiter.BaseSalary;
        }

        /// <summary>
        /// To Delete the selected waiter
        /// </summary>
        public void DeleteWaiter()
        {
            if (this.viewModel.SelectedWaiter == null)
            {
                return;
            }

            Waiter waiter = this.viewModel.SelectedWaiter;
            this.viewModel.AllWaiters.Remove(waiter);
            this.dbLogic.DeleteWaiter(waiter);
        }

        /// <summary>
        /// Get All the Waiter ID in the List of AllWaiters in viewmodel
        /// </summary>
        /// <returns>List of integer numbers</returns>
        public List<int> GetAllWaiterID()
        {
            List<int> ids = new List<int>();
            foreach (Waiter m in this.viewModel.AllWaiters)
            {
                ids.Add(m.Id);
            }

            return ids;
        }

        /// <summary>
        /// Add considering Waiter to the database and viewmodel AllWaiters list
        /// </summary>
        public void AddWaiter()
        {
            if (this.viewModel.WName == null || this.viewModel.WBasedSalary == 0)
            {
                MessageWindow ms = new MessageWindow("Please fill Name and Based Salary");
                ms.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                ms.Show();
            }
            else
            {
                bool check = true;
                foreach (int id in this.GetAllWaiterID())
                {
                    if (this.viewModel.WID == id)
                    {
                        check = false;
                    }
                }

                if (check == false)
                {
                    MessageWindow ms2 = new MessageWindow("Id is not available! Try another");
                    ms2.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    ms2.Show();
                }
                else
                {
                    Waiter waiter = new Waiter();
                    waiter.Name = this.viewModel.WName;
                    waiter.BaseSalary = this.viewModel.WBasedSalary;
                    waiter.Phone = this.viewModel.WPhone;
                    waiter.BankAccount = this.viewModel.WBankAcc;
                    waiter.Address = this.viewModel.WAddress;
                    this.viewModel.AllWaiters.Add(waiter);
                    this.dbLogic.AddWaiter(waiter);
                }
            }
        }

        /// <summary>
        /// Edit the selected WAITER
        /// </summary>
        /// <param name="listview">Name of the listview</param>
        public void EditWaiter(ListView listview)
        {
            if (this.viewModel.SelectedWaiter == null)
            {
                return;
            }
            else
            {
                Waiter w = new Waiter();
                w.Id = this.viewModel.WID;
                w.Name = this.viewModel.WName;
                w.BaseSalary = this.viewModel.WBasedSalary;
                w.Phone = this.viewModel.WPhone;
                w.Address = this.viewModel.WAddress;
                w.BankAccount = this.viewModel.WBankAcc;
                int index = listview.SelectedIndex;
                this.viewModel.AllWaiters.RemoveAt(index);
                this.viewModel.AllWaiters.Insert(index, w);
                this.dbLogic.EditWaiter(w);
            }
        }
    }
}
