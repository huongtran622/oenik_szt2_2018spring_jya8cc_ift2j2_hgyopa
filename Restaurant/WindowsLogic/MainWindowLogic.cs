﻿// <copyright file="MainWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.WindowsLogic
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Media;
    using Data;
    using Logic;
    using ViewModels;

    /// <summary>
    /// Logic part for Main Window
    /// </summary>
    public class MainWindowLogic
    {
        // Declare Main Window Viewmodel
        private MainWindowVM viewModel;

        // Declare and call the Logic in logic layer
        private DBLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowLogic"/> class.
        /// Constructor takes MainWindowVM as paramater
        /// </summary>
        /// <param name="vm">Main Window viewmodel</param>
        public MainWindowLogic(MainWindowVM vm)
        {
            this.viewModel = vm;
            this.logic = new DBLogic();
        }

        /// <summary>
        /// Changed the table button background
        /// </summary>
        /// <param name="tableButton">the button</param>
        /// <param name="brush">the color</param>
        /// <param name="tableId">the table id</param>
        public void OrderedBackground(System.Windows.Controls.Button tableButton, Brush brush, int tableId)
        {
            BindingList<MealOrder> editMealOrders = new BindingList<MealOrder>();

            // Go through the order in tables and search if the table is the button content and the payment status is still false
            foreach (Order o in this.logic.GetAllOrders())
            {
                if (o.TableId == tableId && o.PaymentStatus == false)
                {
                    foreach (MealOrder mo in this.logic.GetAllMealOrders())
                    {
                        if (o.Id == mo.OrderId)
                        {
                            editMealOrders.Add(mo);
                        }
                    }

                    OrderWindow win = new OrderWindow(o, editMealOrders);
                    win.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                    // if the Print is clicked then the button changes to initial background
                    if (win.ShowDialog() == false)
                    {
                        win.Close();
                        tableButton.Background = brush;
                    }
                }
            }
        }
    }
}
