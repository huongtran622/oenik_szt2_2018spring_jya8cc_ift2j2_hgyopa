﻿// <copyright file="OrderWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.WindowsLogic
{
    using System.Collections.Generic;
    using Data;
    using Logic;
    using ViewModels;

    /// <summary>
    /// Logic for Order Window
    /// </summary>
    public class OrderWindowLogic
    {
        // Call the OrderWindow Viewmodel
        private OrderWindowVM vm;

        // Call the logic in logic layer
        private DBLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderWindowLogic"/> class.
        /// Constructor takes OrderWindowVM as parameter
        /// </summary>
        /// <param name="vm">OrderWindowVM</param>
        public OrderWindowLogic(OrderWindowVM vm)
        {
            this.vm = vm;
            this.logic = new DBLogic();
        }

        /// <summary>
        /// Get the Selected Waiter
        /// </summary>
        /// <param name="idwaiter">id of the selected waiter</param>
        /// <returns>Waiter selected</returns>
        public Waiter GetSelectedWaiter(int idwaiter)
        {
            Waiter waiter = new Waiter();
            foreach (Waiter m in this.vm.Waiters)
            {
                if (m.Id == idwaiter)
                {
                    waiter = m;
                }
            }

            return waiter;
        }

        /// <summary>
        /// Get the TotalPrice after add more Meals
        /// </summary>
        /// <returns>the Total Price</returns>
        public double TotalPriceAfter_AddMeal()
        {
            if (this.vm.SelectedMeal == null)
            {
                return 0;
            }

            MealOrder newmealorder = new MealOrder();
            newmealorder.OrderId = this.vm.OrderID;
            newmealorder.MealId = this.vm.SelectedMeal.Id;
            newmealorder.MealName = this.vm.SelectedMeal.Name;
            newmealorder.Quantity = 1;
            newmealorder.Total_Price = this.vm.SelectedMeal.Price;
            this.vm.MealOrders.Add(newmealorder);

            return this.vm.TotalPrice += newmealorder.Total_Price * newmealorder.Quantity;
        }

        /// <summary>
        /// Get the TotalPrice after delete Meals
        /// </summary>
        /// <returns>the total Price</returns>
        public double TotalPriceAfter_DeleteMeal()
        {
            if (this.vm.SelectedMealOrder == null)
            {
                return 0;
            }

            this.vm.TotalPrice = this.vm.TotalPrice - this.vm.SelectedMealOrder.Total_Price;
            this.vm.MealOrders.Remove(this.vm.SelectedMealOrder);
            return this.vm.TotalPrice;
        }

        /// <summary>
        /// Check if the Order is existing or not
        /// </summary>
        /// <param name="id">OrderID</param>
        /// <returns>true if exits, vice versa</returns>
        public bool CheckExistOrder(int id)
        {
            bool checkExist = false;
            foreach (Order order in this.logic.GetAllOrders())
            {
                if (order.Id == id)
                {
                    checkExist = true;
                }
            }

            return checkExist;
        }

        /// <summary>
        /// action when decreasing the quantity of the Meal choosed by 1
        /// </summary>
        public void MinusQuantityMeal()
        {
            if (this.vm.SelectedMealOrder == null)
            {
                return;
            }

            MealOrder mo = this.vm.SelectedMealOrder;
            mo.Quantity--;
            if (mo.Quantity <= 0)
            {
                mo.Quantity = 0;
            }

            foreach (Meal m in this.vm.Meals)
            {
                if (mo.MealId == m.Id)
                {
                    mo.Total_Price = mo.Quantity * m.Price;
                    this.vm.TotalPrice = this.vm.TotalPrice - m.Price;
                }
            }

            this.vm.MealOrders.Remove(this.vm.SelectedMealOrder);
            this.vm.MealOrders.Add(mo);
        }

        /// <summary>
        /// action when increasing the quantity of the Meal choosed by 1
        /// </summary>
        /// <param name="listview">The ListView</param>
        public void PlusQuantityMeal(System.Windows.Controls.ListView listview)
        {
            if (this.vm.SelectedMealOrder == null)
            {
                return;
            }

            MealOrder mo = this.vm.SelectedMealOrder;
            mo.Quantity++;

            foreach (Meal m in this.vm.Meals)
            {
                if (mo.MealId == m.Id)
                {
                    mo.Total_Price = mo.Quantity * m.Price;
                    this.vm.TotalPrice = this.vm.TotalPrice + (m.Price * (mo.Quantity - 1));
                }
            }

            int index = listview.SelectedIndex;
            this.vm.MealOrders.RemoveAt(index);
            this.vm.MealOrders.Insert(index, mo);
        }

        /// <summary>
        /// Save(Edit) the order into the database
        /// </summary>
        /// <param name="idorder">the order id</param>
        /// <param name="currentOrder">the order we are considering</param>
        public void SaveOrderInDatabase(int idorder, Order currentOrder)
        {
            List<Order> orders = this.logic.GetAllOrders();
            foreach (Order order in orders)
            {
                if (order.Id == idorder)
                {
                    this.logic.EditOrder(currentOrder);
                    this.logic.UpdateMealOrders(currentOrder, this.vm.MealOrders);
                }
            }
        }

        /// <summary>
        /// Generate the Invoice from the viewmodel
        /// </summary>
        /// <param name="currentOrder">The order we are considering</param>
        public void GenerateInvoice(Order currentOrder)
        {
            // if the Print button is clicked, change all the payment status to true, it's paid
            this.vm.PaymentStatus = true;
            currentOrder.PaymentStatus = this.vm.PaymentStatus;

            // and also changed it in database as well
            List<Order> orders = this.logic.GetAllOrders();
            foreach (Order order in orders)
            {
                if (order.Id == currentOrder.Id)
                {
                    this.logic.EditOrder(currentOrder);
                }
            }
        }

        /// <summary>
        /// Add new Order to the database
        /// </summary>
        /// <param name="currentOrder">The order we are doing</param>
        public void AddNewToDatabase(Order currentOrder)
        {
            // Add the CurrentOrder to the database
            this.logic.AddOrder(currentOrder);

            // Add all selected Meals into the database
            foreach (MealOrder mo in this.vm.MealOrders)
            {
                mo.OrderId = currentOrder.Id;
                this.logic.AddMealOrder(mo);
            }
        }
    }
}
