﻿// <copyright file="ReportWindowLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.WindowsLogic
{
    using Data;
    using Logic;
    using ViewModels;

    /// <summary>
    /// Logic for ReportWindow
    /// </summary>
    public class ReportWindowLogic
    {
        // Call the viewmodel
        private ReportWindowVM viewModel;

        // Call the logic in the logic layer
        private DBLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportWindowLogic"/> class.
        /// Constructor takes viewmodel as parameter
        /// </summary>
        /// <param name="vm">The ReportWindowVM</param>
        public ReportWindowLogic(ReportWindowVM vm)
        {
            this.logic = new DBLogic();
            this.viewModel = vm;
        }

        /// <summary>
        /// Display all the waiters in the database to the viewmodel
        /// </summary>
        public void DisplayWaiter()
        {
            foreach (Waiter waiter in this.logic.GetAllWaiters())
            {
                waiter.TotalSalary = 0;
                foreach (Order or in this.logic.GetAllOrders())
                {
                    if (waiter.Id == or.WaiterId)
                    {
                        waiter.TotalSalary += (double)or.Tip;
                    }
                }

                this.viewModel.AllWaiters.Add(waiter);
            }
        }

        /// <summary>
        /// Calculate the total income
        /// </summary>
        /// <returns>the total income</returns>
        public double CalculateIncome()
        {
            if (this.logic.GetAllOrders().Count > 0)
            {
                return this.logic.GetTotalIncome();
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Calculate the total tip got after every orders
        /// </summary>
        /// <returns>total tip</returns>
        public double CalculateTotalTip()
        {
            if (this.logic.GetAllOrders().Count > 0)
            {
                return this.logic.GetTotalTip();
            }
            else
            {
                return 0;
            }
        }
    }
}
