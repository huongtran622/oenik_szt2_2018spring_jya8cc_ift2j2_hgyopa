﻿// <copyright file="OrderWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System;
    using System.ComponentModel;
    using System.Windows;
    using Data;
    using Logic;
    using ViewModels;
    using WindowsLogic;

    /// <summary>
    /// Interaction logic for MealOrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        // Call the viewmodel
        private OrderWindowVM viewModel;

        // Call the logic in logic layer
        private DBLogic logic;

        // Call the logic of the Order Window
        private OrderWindowLogic orderLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderWindow"/> class.
        /// Start the Window when we know only the table and order id
        /// </summary>
        /// <param name="tableId">Table id</param>
        /// <param name="idorder">order ID</param>
        public OrderWindow(int tableId, int idorder)
        {
            this.InitializeComponent();
            this.CurrentOrder = new Order();
            this.AllMealOrders = new BindingList<MealOrder>();
            this.CurrentOrder.Id = idorder + 1;
            this.CurrentOrder.TableId = tableId;
            this.CurrentOrder.DateTime = DateTime.Now;
            this.viewModel = new OrderWindowVM();
            this.viewModel.TableID = this.CurrentOrder.TableId;
            this.viewModel.OrderID = this.CurrentOrder.Id;
            this.viewModel.Datetime = this.CurrentOrder.DateTime.ToString();

            this.logic = new DBLogic();
            this.orderLogic = new OrderWindowLogic(this.viewModel);
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderWindow"/> class.
        /// Start the Window when we know the order and list of meal orders
        /// </summary>
        /// <param name="order">Order</param>
        /// <param name="mealorder">List of MealOrders</param>
        public OrderWindow(Order order, BindingList<MealOrder> mealorder)
        {
            this.InitializeComponent();
            this.viewModel = new OrderWindowVM();

            this.logic = new DBLogic();
            this.viewModel.MealOrders = mealorder;
            this.AllMealOrders = mealorder;
            this.viewModel.CheckSave = true;
            this.viewModel.CheckCancel = false;
            this.viewModel.CheckBook = false;

            this.CurrentOrder = order;
            this.viewModel.TableID = this.CurrentOrder.TableId;
            this.viewModel.OrderID = this.CurrentOrder.Id;
            this.viewModel.TipOrder = (double)this.CurrentOrder.Tip;
            this.viewModel.Datetime = this.CurrentOrder.DateTime.ToString();
            this.viewModel.TotalPrice = this.CurrentOrder.TotalPrice;
            this.viewModel.PaymentStatus = this.CurrentOrder.PaymentStatus;
            this.viewModel.PaymentType = this.CurrentOrder.PaymentType;
            this.viewModel.CustomerFeedback = this.CurrentOrder.CustomerFeedback;

            this.orderLogic = new OrderWindowLogic(this.viewModel);
            this.viewModel.SelectedWaiter = this.orderLogic.GetSelectedWaiter(this.CurrentOrder.WaiterId);
            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// Gets or sets the current order
        /// </summary>
        public Order CurrentOrder { get; set; }

        /// <summary>
        /// Gets or sets the all meal orders
        /// </summary>
        public BindingList<MealOrder> AllMealOrders { get; set; }

        /// <summary>
        /// When the AddMeal button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void AddMeal_Click(object sender, RoutedEventArgs e)
        {
            this.CurrentOrder.TotalPrice = this.orderLogic.TotalPriceAfter_AddMeal();
        }

        /// <summary>
        /// When the DeleteMeal button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void DeleteMeal_Click(object sender, RoutedEventArgs e)
        {
            this.CurrentOrder.TotalPrice = this.orderLogic.TotalPriceAfter_DeleteMeal();
        }

        /// <summary>
        /// When the BOOK button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.SelectedWaiter == null)
            {
                MessageWindow ms = new MessageWindow("Please choose the Waiter!");
                ms.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                ms.Show();
            }
            else
            {
                this.CurrentOrder.WaiterId = this.viewModel.SelectedWaiter.Id;
                this.CurrentOrder.CustomerFeedback = this.viewModel.CustomerFeedback;
                this.CurrentOrder.Tip = this.viewModel.TipOrder;
                this.CurrentOrder.PaymentStatus = this.viewModel.PaymentStatus;
                this.CurrentOrder.PaymentType = this.viewModel.PaymentType;
                this.CurrentOrder.TotalPrice = this.viewModel.TotalPrice;

                // after click ok, add new order and mealorders to database
                this.orderLogic.AddNewToDatabase(this.CurrentOrder);
                this.AllMealOrders = this.viewModel.MealOrders;
                this.DialogResult = true;
            }
        }

        /// <summary>
        /// When the Invoice button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Invoice_Click(object sender, RoutedEventArgs e)
        {
            InvoiceWindow invoice = new InvoiceWindow(this.viewModel);
            invoice.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            if (invoice.ShowDialog() == true)
            {
                this.orderLogic.GenerateInvoice(this.CurrentOrder);
                this.DialogResult = false;
            }
        }

        /// <summary>
        /// When the Minus button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            this.orderLogic.MinusQuantityMeal();
        }

        /// <summary>
        /// When the Plus button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            this.orderLogic.PlusQuantityMeal(this.ListViewBox);
        }

        /// <summary>
        /// When the Save button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.CurrentOrder.WaiterId = this.viewModel.SelectedWaiter.Id;
            this.CurrentOrder.CustomerFeedback = this.viewModel.CustomerFeedback;
            this.CurrentOrder.Tip = this.viewModel.TipOrder;
            this.CurrentOrder.PaymentStatus = this.viewModel.PaymentStatus;
            this.CurrentOrder.PaymentType = this.viewModel.PaymentType;
            this.CurrentOrder.TotalPrice = this.viewModel.TotalPrice;

            this.AllMealOrders = this.viewModel.MealOrders;

            // save changes in database too!
            this.orderLogic.SaveOrderInDatabase(this.CurrentOrder.Id, this.CurrentOrder);
            this.DialogResult = true;
        }

        /// <summary>
        /// When the Cancel button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}