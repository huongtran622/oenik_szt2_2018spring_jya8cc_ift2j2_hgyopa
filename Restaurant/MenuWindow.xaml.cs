﻿// <copyright file="MenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;
    using Data;
    using Logic;
    using ViewModels;
    using WindowsLogic;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        // Call the viewmodel
        private MenuWindowVM viewModel;

        // Call the logic in logic layer
        private DBLogic dbLogic;

        // Call the Logic of the MenuWindow
        private MenuWindowLogic menulogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// Start up the window
        /// </summary>
        public MenuWindow()
        {
            this.InitializeComponent();
            this.viewModel = new MenuWindowVM();
            this.menulogic = new MenuWindowLogic(this.viewModel);
            this.dbLogic = new DBLogic();
            List<Category> categories = this.dbLogic.GetAllCategories();
            this.viewModel.Categories = new BindingList<Category>(categories);
            List<Meal> meals = this.dbLogic.GetAllMeals();
            this.viewModel.Meals = new BindingList<Meal>(meals);
            List<Waiter> waiters = this.dbLogic.GetAllWaiters();
            this.viewModel.AllWaiters = new BindingList<Waiter>(waiters);

            this.DataContext = this.viewModel;
        }

        /// <summary>
        /// When the Add button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.AddMeal();
        }

        /// <summary>
        /// When the Save button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.EditMeal(this.MealsListBox);
        }

        /// <summary>
        /// When the Edit button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.ClearAll(this.idTextbox, this.nameTextBox, this.descTextBox, this.cateTextBox, this.priceTextBox);
            this.menulogic.DisplaySelectedMeal();
        }

        /// <summary>
        /// When the Delete button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.DeleteMeal();
            this.menulogic.ClearAll(this.idTextbox, this.nameTextBox, this.descTextBox, this.cateTextBox, this.priceTextBox);
        }

        /// <summary>
        /// When the Exit button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// When the Clear button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event handler</param>
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.ClearAll(this.idTextbox, this.nameTextBox, this.descTextBox, this.cateTextBox, this.priceTextBox);
        }

        /// <summary>
        /// When the Edit waiter button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event Handler</param>
        private void EditWaiter_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.ClearAllWaitersTextBox(this.IDWaiterTb, this.NameTb, this.PhoneTb, this.AddressTb, this.BankAccTb, this.BasedSalTb);
            this.menulogic.DisplaySelectedWaiter();
        }

        /// <summary>
        /// When the Delete waiter button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event Handler</param>
        private void DelWaiter_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.DeleteWaiter();
        }

        /// <summary>
        /// When the Clear button in the Waiter tab is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event Handler</param>
        private void WClear_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.ClearAllWaitersTextBox(this.IDWaiterTb, this.NameTb, this.PhoneTb, this.AddressTb, this.BankAccTb, this.BasedSalTb);
        }

        /// <summary>
        /// When the Add waiter button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event Handler</param>
        private void AddWaiter_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.AddWaiter();
        }

        /// <summary>
        /// When the Save waiter button is clicked
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">event Handler</param>
        private void SaveWaiter_Click(object sender, RoutedEventArgs e)
        {
            this.menulogic.EditWaiter(this.WaitersListView);
        }
    }
}
