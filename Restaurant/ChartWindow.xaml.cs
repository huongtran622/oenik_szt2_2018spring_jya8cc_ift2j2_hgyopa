﻿// <copyright file="ChartWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ChartWindow.xaml
    /// </summary>
    public partial class ChartWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChartWindow"/> class.
        /// initalize
        /// </summary>
        public ChartWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Take action when Exit button is click
        /// </summary>
        /// <param name="sender">button</param>
        /// <param name="e">even handler</param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
