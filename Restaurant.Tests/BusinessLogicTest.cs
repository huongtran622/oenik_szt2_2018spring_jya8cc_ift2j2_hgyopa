﻿// <copyright file="BusinessLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Restaurant.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Logic;
    using NUnit.Framework;

    /// <summary>
    /// Class
    /// </summary>
    [TestFixture]
    public class BusinessLogicTest
    {
        private ILogic logic;

        /// <summary>
        /// Initializes objects used in test methods
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.logic = new DBLogic();
        }

        /// <summary>
        /// Tests the insertion of an input
        /// </summary>
        [Test]
        public void InsertNewItemTest()
        {
            Category c = new Category()
            {
                Name = "WINE",
            };

            this.logic.AddCategory(c);

            Assert.That(this.logic.GetAllCategories()[this.logic.GetAllCategories().Count - 1].Name, Is.EqualTo("WINE"));
            this.logic.DeleteCategory(c);
        }

        /// <summary>
        /// Tests the insertion of a waiter without name
        /// </summary>
        [Test]
        public void InsertWaiterWithoutNameTest()
        {
            Waiter w = new Waiter
            {
                BaseSalary = 1000,
                Address = "Budapest Bécsi út 96/b",
            };

            Assert.That(() => this.logic.AddWaiter(w), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a waiter without base salary
        /// </summary>
        [Test]
        public void InsertWaiterWithoutBaseSalaryTest()
        {
            Waiter w = new Waiter
            {
                Name = "John",
                Address = "Budapest Bécsi út 96/b",
            };

            Assert.That(() => this.logic.AddWaiter(w), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a category without name
        /// </summary>
        [Test]
        public void InsertCategoryWithoutNameTest()
        {
            Category c = new Category();

            Assert.That(() => this.logic.AddCategory(c), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of null input
        /// </summary>
        [Test]
        public void InsertNullTest()
        {
            Assert.That(() => this.logic.AddCategory(null), Throws.Exception.TypeOf<ArgumentNullException>());
        }

        /// <summary>
        /// Tests the insertion of an input twice
        /// </summary>
        [Test]
        public void DuplicateDataTest()
        {
            Category c = new Category()
            {
                Name = "Beer",
            };

            this.logic.AddCategory(c);
            Assert.That(() => this.logic.AddCategory(c), Throws.Exception.TypeOf<ApplicationException>());

            this.logic.DeleteCategory(c);
        }

        /// <summary>
        /// Tests the insertion of a mealOrder without order id
        /// </summary>
        [Test]
        public void InsertMealOrderWithoutOrderTest()
        {
            MealOrder mo = new MealOrder()
            {
                MealId = 2,
                Quantity = 10,
                Total_Price = 1500,
            };

            Assert.That(() => this.logic.AddMealOrder(mo), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a mealOrder without meal id
        /// </summary>
        [Test]
        public void InsertMealOrderWithoutMealTest()
        {
            MealOrder mo = new MealOrder()
            {
                OrderId = 2,
                Quantity = 10,
                Total_Price = 1500,
            };

            Assert.That(() => this.logic.AddMealOrder(mo), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a mealOrder without total price
        /// </summary>
        [Test]
        public void InsertMealOrderWithoutTotalPriceTest()
        {
            MealOrder mo = new MealOrder()
            {
                MealId = 2,
                OrderId = 2,
                Quantity = 10,
            };

            Assert.That(() => this.logic.AddMealOrder(mo), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a mealOrder without quantity
        /// </summary>
        [Test]
        public void InsertMealOrderWithoutQuantityTest()
        {
            MealOrder mo = new MealOrder()
            {
                MealId = 2,
                OrderId = 2,
                Total_Price = 1000,
            };

            Assert.That(() => this.logic.AddMealOrder(mo), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a meal without name
        /// </summary>
        [Test]
        public void InsertMealWithoutNameTest()
        {
            Meal m = new Meal()
            {
                CategoryId = 2,
                Price = 2000,
            };

            Assert.That(() => this.logic.AddMeal(m), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a meal without category
        /// </summary>
        [Test]
        public void InsertMealWithoutCategoryTest()
        {
            Meal m = new Meal()
            {
                Name = "Pasta",
                Price = 2000,
            };

            Assert.That(() => this.logic.AddMeal(m), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of a meal without price
        /// </summary>
        [Test]
        public void InsertMealWithoutPriceTest()
        {
            Meal m = new Meal()
            {
                CategoryId = 2,
                Name = "Pasta",
            };

            Assert.That(() => this.logic.AddMeal(m), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of an order without table
        /// </summary>
        [Test]
        public void InsertOrderWithoutTableTest()
        {
            Order o = new Order
            {
                WaiterId = 1,
                PaymentStatus = false,
                TotalPrice = 1500,
                DateTime = DateTime.Now,
                Tip = 500,
            };

            Assert.That(() => this.logic.AddOrder(o), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of an order without waiter
        /// </summary>
        [Test]
        public void InsertOrderWithoutWaiterTest()
        {
            Order o = new Order
            {
                TableId = 1,
                PaymentStatus = false,
                TotalPrice = 1500,
                DateTime = DateTime.Now,
                Tip = 500,
            };

            Assert.That(() => this.logic.AddOrder(o), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the insertion of an order without totalprice
        /// </summary>
        [Test]
        public void InsertOrderWithoutTotalPriceTest()
        {
            Order o = new Order
            {
                TableId = 1,
                PaymentStatus = false,
                WaiterId = 1,
                DateTime = DateTime.Now,
                Tip = 500,
            };

            Assert.That(() => this.logic.AddOrder(o), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the passing of a null input to update function
        /// </summary>
        [Test]
        public void UpdateNullTest()
        {
            Assert.That(() => this.logic.EditWaiter(null), Throws.Exception.TypeOf<ArgumentNullException>());
        }

        /// <summary>
        /// Tests the passing of a correct input to delete function
        /// </summary>
        [Test]
        public void DeleteDataTest()
        {
            Category c = new Category() { Name = "ToDelete" };
            this.logic.AddCategory(c);
            int count = this.logic.GetAllCategories().Count();

            this.logic.DeleteCategory(c);
            Assert.That(this.logic.GetAllCategories().Count, Is.EqualTo(count - 1));
        }

        /// <summary>
        /// Tests the passing of a null input to delete function
        /// </summary>
        [Test]
        public void DeleteNullTest()
        {
            Assert.That(() => this.logic.DeleteWaiter(null), Throws.Exception.TypeOf<ArgumentNullException>());
        }

        /// <summary>
        /// Tests the passing of a not existing input to delete function
        /// </summary>
        [Test]
        public void DeleteNotExistingTest()
        {
            Waiter w = new Waiter() { Name = "ToDelete" };

            Assert.That(() => this.logic.DeleteWaiter(w), Throws.Exception.TypeOf<ApplicationException>());
        }

        /// <summary>
        /// Tests the calculation of the total amount of tip
        /// </summary>
        [Test]
        public void TotalTipTest()
        {
            var totalTip = this.logic.GetTotalTip();
            Order o = new Order
            {
                TableId = 1,
                WaiterId = 1,
                PaymentStatus = false,
                TotalPrice = 1500,
                DateTime = DateTime.Now,
                Tip = 500,
            };

            this.logic.AddOrder(o);
            Assert.That(this.logic.GetTotalTip(), Is.EqualTo(totalTip + o.Tip));
        }

        /// <summary>
        /// Tests the calculation of the total amount of income
        /// </summary>
        [Test]
        public void TotalIncomeTest()
        {
            var totalIncome = this.logic.GetTotalIncome();
            Order o = new Order
            {
                TableId = 1,
                WaiterId = 1,
                PaymentStatus = false,
                TotalPrice = 1500,
                DateTime = new DateTime(2010, 10, 12),
                Tip = 500,
            };

            this.logic.AddOrder(o);
            Assert.That(this.logic.GetTotalIncome(), Is.EqualTo(totalIncome + o.TotalPrice));
        }
    }
}
